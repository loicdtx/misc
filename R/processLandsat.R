# Loic Dutrieux
# December 2013

# Wrapper function to batch process NDVI from the tarball,
# The idea is to run this function with apply, or mclapply over the list of tarball

# Dependencies: hdf2ndvi

processLandsat <- function(x, hdfdir, outdir, untar=TRUE, delete=FALSE, ...) {
    # x is the full path of a tarball containing the Landsat data
    # hdf dir is where the hdf files are extracted
    # Output layers (NDVI for example) are generated in outdir
    # ... arguments to be passed to hdf2ndvi (filename is automatically generated and therefore does not need to be passed)
    if(untar){
        untar(x, exdir=hdfdir, files='*.hdf', extras='--wildcards') # TODO This unpacking is surprisingly slow ... maybe system tar will be faster (platform dependent though)
    }
    name <- substr(basename(x), 1, nchar(basename(x)) - 24)
    hdf <- list.files(hdfdir, pattern=glob2rx(sprintf('lndsr.%s*.hdf', name)), full.names=TRUE)
    hdf2ndvi(x=hdf, filename=sprintf('%s/ndvi.%s', outdir, name), datatype='INT2S', format='ENVI', ...)
    if(delete) {
        file.remove(hdf)
    } 
}