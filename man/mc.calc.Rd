\name{mc.calc}
\alias{mc.calc}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Multicore implementation of the raster::\code{\link{calc}} function.
}
\description{
Allow functions to be applied to raster objects, with multicore support.
}
\usage{
mc.calc(x, fun, mc.cores = 1, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
Raster object.
}
  \item{fun}{
Function to be applied to the raster object.
}
  \item{mc.cores}{
Number of cores to be used during the calculation.
}
  \item{\dots}{
Arguments to be passed to \code{\link{writeRaster}}; only \code{filename} and \code{overwrite} are supported at the moment.
}
}
\details{
For further help, see \code{\link{calc}}.
Warnings of the parallel package (see \code{\link{mclapply}} for instance) apply to this function.
}
\value{
Raster object
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Loic Dutrieux
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{calc}}
}
\examples{
## TODO: write example
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ calc }
\keyword{ raster }% __ONLY ONE__ keyword per line
